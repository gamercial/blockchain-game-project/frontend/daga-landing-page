import styled from 'styled-components';
import { Button } from 'components/Button';
import { composedStyles, ComposedProps, majorScalePx } from 'components/utils';
import { IntroductionInfoProps } from '../types';
import GameplayIntroductionHeader from './GameplayIntroductionHeader';
import GameplayIntroductionDescription from './GameplayIntroductionDescription';
import GameplayIntroductionFeatures from './GameplayIntroductionFeatures';

const IntroductionInfoContainer = styled.div<ComposedProps>`
	float: none;
	height: 100%;
	padding-top: ${majorScalePx(8)};
	padding-left: ${majorScalePx(4)};
	width: 100%;
	${composedStyles}
	${({ theme }) => theme.mediaQueries.sm} {
		float: right;
		max-width: 551px;
		padding-top: 0;
		padding-left: 0;
		width: 50%;
	}
`;

const Content = styled.div`
	display: inline-block;
	height: fit-content;
`;

function GameplayIntroductionInfo(props: IntroductionInfoProps) {
	const onClickPlayDemoGame = (): void => {
		window.location.href = './coming-soon';
	};

	return (
		<IntroductionInfoContainer>
			<GameplayIntroductionHeader />
			<GameplayIntroductionDescription>
				Battle is a Turn-based Strategy Game with elements of Collectible Card
				and Pet Battle. Players lead teams of 3 Gallumon to fight with NPC or
				other players. Each Gallumon is comprised of body parts, which determine
				their stats and unique ability cards
			</GameplayIntroductionDescription>
			<GameplayIntroductionFeatures />
			<Button onClick={onClickPlayDemoGame} variant={'secondary'}>
				PLAY DEMO GAME
			</Button>
		</IntroductionInfoContainer>
	);
}

export default GameplayIntroductionInfo;
