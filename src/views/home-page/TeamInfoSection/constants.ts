import { getPublicAvatarMember } from 'utils/getResource';

const gen = (function* generator(index: number) {
	while (true) yield ++index;
})(0);

function getId() {
	return gen.next().value;
}

export const TeamMembers = [
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('brian.webp'),
		link: 'https://www.linkedin.com/in/brian-tran-a68210213/',
		name: 'Brian',
		position: 'Founder',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('khanh-le.jpg'),
		link: 'https://www.linkedin.com/in/khanhlp',
		name: 'Khanh Le',
		position: 'Co-Founder & CTO',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('hieu-bui.jpg'),
		link: 'https://www.linkedin.com/in/hieu-bui-22036815b/',
		name: 'Hieu Bui',
		position: 'Lead Artist',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('kien-ngo.jpg'),
		link: 'https://www.linkedin.com/in/kien-ngo-039120171/',
		name: 'Kraken',
		position: 'Product Manager',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('phat-pham.jpg'),
		link: 'https://www.linkedin.com/in/nemo-the-collector-133325223/',
		name: 'Nemo',
		position: 'FE/Blockchain Engineer',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('khang-le.jpg'),
		link: 'https://www.linkedin.com/in/levogiakhang/',
		name: 'Jaka',
		position: 'FE Engineer',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('khuong-nguyen.jpg'),
		link: 'https://www.linkedin.com/in/khuongnb97/',
		name: 'Khuong Nguyen',
		position: 'Blockchain Engineer',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('hoang-minh.jpg'),
		link: 'https://www.linkedin.com/in/adam-wu-9389a6157/',
		name: 'Hoang Minh',
		position: 'Game Developer',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('hoang-nguyen.jpg'),
		link: 'https://www.linkedin.com/in/nguyen-hoang-317a51222/',
		name: 'Hoang Nguyen',
		position: '2D Concept Artist',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('nhan-dao.jpg'),
		link: 'https://www.linkedin.com/in/nhan-trong-70a1aa223/',
		name: 'Nhan Dao',
		position: 'Marketing Artist',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('hau-phan.jpg'),
		link: 'https://www.linkedin.com/in/hanja-phan-6576041b8/',
		name: 'Hau Phan',
		position: '2D Artist',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('thai-tran.jpg'),
		link: 'https://www.linkedin.com/in/ben-tran-1a073977/',
		name: 'Thai Tran',
		position: '2D Concept Artist',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('duc-tran.jpg'),
		link: 'https://www.linkedin.com/in/duc-tran-ngoc-43441ba3/',
		name: 'Duc Tran',
		position: '3D Artist',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('khoi-le.jpg'),
		link: 'https://www.linkedin.com/in/khoi-le-3danimator/',
		name: 'Khoi Le',
		position: '3D Animator',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('phung-la.jpg'),
		link: 'https://www.linkedin.com/in/lakimphung/',
		name: 'Phung La',
		position: 'Technical & VFX artist',
	},
	{
		id: getId(),
		imageSrc: getPublicAvatarMember('lap-hong.jpg'),
		link: 'https://www.linkedin.com/in/gia-l%E1%BA%ADp-h%E1%BB%93ng-606473205/',
		name: 'Lap Hong',
		position: 'Game Designer',
	},
];
