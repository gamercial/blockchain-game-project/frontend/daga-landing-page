import styled from 'styled-components';
import { LayoutCenter, majorScalePx } from 'components/utils';
import { Image } from 'components/Image';
import { Typography } from 'components/Typography';
import { TeamMemberCardProps } from './types';

const TeamMemberCardContainer = styled.a`
	background-color: ${({ theme }) => theme.colors.gray500};
	border-radius: 8px;
	cursor: pointer;
	display: block;
	padding: ${majorScalePx(4)};
	height: 396px;
	width: 264px;
`;

const ImageContainer = styled.div`
	margin-bottom: ${majorScalePx(4)};
	${LayoutCenter};
`;

const NameContainer = styled.div`
	margin-bottom: ${majorScalePx(1)};
	${LayoutCenter};
`;

const PositionContainer = styled.div`
	${LayoutCenter};
`;

function TeamMemberCard(props: TeamMemberCardProps): JSX.Element {
	const { imageSrc, link, name, position } = props;
	return (
		<TeamMemberCardContainer href={link} title={link} target={'_blank'}>
			<ImageContainer>
				<Image
					alt={'MemberAva'}
					borderRadius={8}
					height={264}
					objectFit={'cover'}
					src={imageSrc}
					width={232}
				/>
			</ImageContainer>
			<NameContainer>
				<Typography variant={'h4'}>{name}</Typography>
			</NameContainer>
			<PositionContainer>
				<Typography variant={'h5'} color={'#0CEFA7'}>
					{position}
				</Typography>
			</PositionContainer>
		</TeamMemberCardContainer>
	);
}

export default TeamMemberCard;
