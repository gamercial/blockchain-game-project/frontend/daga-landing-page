import styled from 'styled-components';
import { LayoutCenter, majorScalePx } from 'components/utils';
import { getPublicImageResource } from 'utils/getResource';
import GameCardItem from './GameCardItem';
import { GameCardsProps } from './types';

const GameCardsContainer = styled.div`
	display: block;
	padding-bottom: ${majorScalePx(16)};
	${({ theme }) => theme.mediaQueries.sm} {
		padding-bottom: ${majorScalePx(39)};
	}
`;

const GameCardItemsContainer = styled.div`
	${LayoutCenter}
`;

const GameCardItemsScroller = styled.div`
	display: flex;
	overflow-x: scroll;
	::-webkit-scrollbar {
		display: none;
	}
`;

function GameCards(props: GameCardsProps): JSX.Element {
	const { items } = props;
	return (
		<GameCardsContainer>
			<GameCardItemsContainer>
				<GameCardItemsScroller>
					{items.map((item, index) => (
						<GameCardItem
							key={item.id}
							backgroundImage={getPublicImageResource(item.fileName)}
							item={item}
							index={index}
						/>
					))}
				</GameCardItemsScroller>
			</GameCardItemsContainer>
		</GameCardsContainer>
	);
}

GameCards.defaultProps = {
	items: [
		{ id: 1, fileName: 'game-introduction-hero-mazter.webp' },
		{ id: 2, fileName: 'game-introduction-hero-gangzter.webp' },
		{ id: 3, fileName: 'game-introduction-hero-toptier.webp' },
		{ id: 4, fileName: 'game-introduction-hero-swagger.webp' },
	],
};

export default GameCards;
