import { ethers } from 'ethers';
import { getRpcNodeUrl } from './getRpcNodeUrl';

const RPC_URL = getRpcNodeUrl();

export const simpleRpcProvider = new ethers.providers.JsonRpcProvider(RPC_URL);

// eslint-disable-next-line import/no-anonymous-default-export
export default null;
