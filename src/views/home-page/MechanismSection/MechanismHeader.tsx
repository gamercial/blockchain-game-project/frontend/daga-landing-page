import SectionHeader from '../components/SectionHeader';

function MechanismHeader(): JSX.Element {
	return (
		<SectionHeader
			backgroundName={'mechanism-section-background.png'}
			title={'NFT / Blockchain'}
		/>
	);
}

export default MechanismHeader;
