export type MechanismItem = {
	id: number | string;
	title: string;
	description: string;
};

export type MechanismItemProps = {
	index: number;
	item: MechanismItem;
};

export type MechanismContentProps = {
	items: Array<MechanismItem>;
};
