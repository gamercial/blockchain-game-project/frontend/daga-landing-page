import styled from 'styled-components';
import GridLayout from 'components/Layout/GridLayout';
import { Image } from 'components/Image';
import { LayoutCenter, majorScalePx } from 'components/utils';
import { getPublicImageResource } from 'utils/getResource';
import PartnerItem from './PartnerItem';
import { PaddingXResponsive } from '../constant';
import { PartnerListProps } from './types';

const PartnerListContainer = styled.div`
	${LayoutCenter};
	padding-top: ${majorScalePx(16)};
	padding-bottom: ${majorScalePx(20)};

	${({ theme }) => theme.mediaQueries.sm} {
		padding-top: ${majorScalePx(30)};
		padding-bottom: ${majorScalePx(40)};
	}
`;

export const PartnerListContentContainer = styled.div`
	background-image: url(${getPublicImageResource(
		'main-background.webp'
	)});
	background-size: 90%;
	background-repeat: no-repeat;
	background-position-y: -35px;
	border: 2px solid #55535b;
	border-radius: 24px;
	padding-top: ${majorScalePx(5)};
	padding-bottom: ${majorScalePx(7)};
	position: relative;

	${({ theme }) => theme.mediaQueries.sm} {
		padding-top: ${majorScalePx(13)};
		padding-bottom: ${majorScalePx(16)};
		width: 100%;
	}

	${LayoutCenter}
`;

export const LeftHeroContainer = styled.div`
	bottom: -10px;
	height: 254px;
	left: -5px;
	position: absolute;
	width: 222px;
	z-index: 1;
`;

export const RightHeroContainer = styled.div`
	bottom: -10px;
	height: 191px;
	right: -5px;
	position: absolute;
	width: 174px;
	z-index: 1;
`;

function PartnerList(props: PartnerListProps): JSX.Element {
	const { partners } = props;
	return (
		<PartnerListContainer>
			<PartnerListContentContainer>
				<LeftHeroContainer>
					<Image
						alt={'LeftHero'}
						height={254}
						src={getPublicImageResource('partner-hero-1.webp')}
						width={222}
					/>
				</LeftHeroContainer>
				<GridLayout
					col={{
						xs: 2,
						sm: 4,
					}}
					justifyItems={'center'}
					gap={{
						xs: 14,
						sm: 24,
					}}
				>
					{partners.map((partner) => (
						<PartnerItem key={partner.id} item={partner} />
					))}
				</GridLayout>
				<RightHeroContainer>
					<Image
						alt={'LeftHero'}
						height={191}
						src={getPublicImageResource('partner-hero-2.webp')}
						width={174}
					/>
				</RightHeroContainer>
			</PartnerListContentContainer>
		</PartnerListContainer>
	);
}

PartnerList.defaultProps = {
	partners: [],
};

export default PartnerList;
