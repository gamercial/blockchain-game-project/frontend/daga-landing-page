import styled from 'styled-components';
import { getPublicImageResource } from 'utils/getResource';
import { Image } from 'components/Image';
import { majorScalePx } from 'components/utils';

const HeroPreviewIntroductionContainer = styled.div`
	box-sizing: border-box;
	float: none;
	padding-left: ${majorScalePx(4)};
	padding-right: ${majorScalePx(4)};
	width: 100%;
	${({ theme }) => theme.mediaQueries.sm} {
		float: left;
		max-width: 551px;
		padding-left: 0;
		padding-right: 0;
		width: 50%;
	}
`;

function HeroPreviewIntroduction() {
	return (
		<HeroPreviewIntroductionContainer>
			<Image
				alt={'HeroPreviewIntroduction'}
				src={getPublicImageResource('game-introduction-hero.webp')}
				height={552}
				width={551}
			/>
		</HeroPreviewIntroductionContainer>
	);
}

export default HeroPreviewIntroduction;
