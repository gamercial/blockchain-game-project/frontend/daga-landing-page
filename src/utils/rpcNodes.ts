export const rpcNodes = [
	process.env.NEXT_PUBLIC_RPC_NODE_1 as string,
	process.env.NEXT_PUBLIC_RPC_NODE_2 as string,
	process.env.NEXT_PUBLIC_RPC_NODE_3 as string,
];
