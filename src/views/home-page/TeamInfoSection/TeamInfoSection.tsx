import styled from 'styled-components';
import TeamInfoHeader from './TeamInfoHeader';
import TeamInfoContent from './TeamInfoContent';
import { TeamMembers } from './constants';
import { TeamInfoSectionProps } from './types';

const TeamInfoSectionContainer = styled.section``;

function TeamInfoSection(props: TeamInfoSectionProps): JSX.Element {
	return (
		<TeamInfoSectionContainer>
			<TeamInfoHeader />
			<TeamInfoContent members={TeamMembers} />
		</TeamInfoSectionContainer>
	);
}

export default TeamInfoSection;
