import { getAddress } from './getAddress';
import addresses from 'configs/constants/contractAddresses';

export const getDagaSystemInfoAddress = () => {
	return getAddress(addresses.dagaSystemInfo);
};
