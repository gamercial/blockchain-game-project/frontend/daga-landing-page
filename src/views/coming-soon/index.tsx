import { HelmetProvider } from 'react-helmet-async';
import { ThemeProvider } from 'styled-components';
import GlobalStyle from 'style/Global';
import { dark } from 'components/theme';
import ComingSoon from './ComingSoon';

function Root(): JSX.Element {
	return (
		<HelmetProvider>
			<ThemeProvider theme={dark}>
				<GlobalStyle />
				<ComingSoon />
			</ThemeProvider>
		</HelmetProvider>
	);
}

export default Root;
