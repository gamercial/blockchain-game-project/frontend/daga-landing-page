import styled from 'styled-components';
import { BoxContainer } from 'components/Box';
import { LayoutCenter, majorScalePx } from 'components/utils';
import { PaddingXResponsive } from 'views/home-page/constant';
import PreorderInfo from './PreorderInfo';
import HeroPreview from './HeroPreview';
import { getPublicImageResource } from 'utils/getResource';
import { PreorderTitle, PreorderDescription } from './fragments';

const PreorderSectionContainer = styled.section`
	background-image: url(${getPublicImageResource(
		'preorder-section-background.webp'
	)});
	background-size: cover;
	background-repeat: repeat;
	background-position: top;
	padding-top: ${majorScalePx(30)};
	padding-bottom: ${majorScalePx(10)};
	width: 100%;
	${({ theme }) => theme.mediaQueries.sm} {
		background-size: contain;
		padding-top: ${majorScalePx(48)};
		padding-bottom: ${majorScalePx(24)};
	}
	${PaddingXResponsive};
	${LayoutCenter};
`;

function PreorderSection({
	preorderTimeInMilSec,
}: {
	preorderTimeInMilSec: number;
}) {
	return (
		<PreorderSectionContainer>
			<BoxContainer>
				<HeroPreview />
				<PreorderInfo
					title={<PreorderTitle />}
					description={<PreorderDescription />}
					preorderTime={1639493714000}
				/>
			</BoxContainer>
		</PreorderSectionContainer>
	);
}

export default PreorderSection;
