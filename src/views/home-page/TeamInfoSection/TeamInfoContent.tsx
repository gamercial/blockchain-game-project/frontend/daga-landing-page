import React, { useState } from 'react';
import styled from 'styled-components';
import { space, SpaceProps } from 'styled-system';
import { BoxContainer } from 'components/Box';
import { ChevronLeftIcon, ChevronRightIcon } from 'components/Icon';
import GridLayout from 'components/Layout/GridLayout';
import { LayoutCenter, majorScalePx } from 'components/utils';
import { getPublicImageResource } from 'utils/getResource';
import { TeamInfoContentProps } from './types';
import TeamMemberCard from './TeamMemberCard';

const MemberCardIdPrefix: string = 'member-card-';

const TeamInfoContentContainer = styled.div`
	background-image: url(${getPublicImageResource(
		'main-background.webp'
	)});
	background-size: cover;
	background-repeat: repeat;
	background-position-y: -320px;
	padding-top: ${majorScalePx(16)};
	padding-bottom: ${majorScalePx(16)};
	${({ theme }) => theme.mediaQueries.sm} {
		padding-top: ${majorScalePx(30)};
		padding-bottom: ${majorScalePx(30)};
	}
`;

const MainContentContainer = styled.div``;

const ListMembersContainer = styled.div`
	display: flex;
	overflow-x: scroll;
	${({ theme }) => theme.mediaQueries.sm} {
		overflow-x: hidden;
	}
	::-webkit-scrollbar {
		height: 0; // Hide horizontal scrollbar
	}
`;

const MemberContainer = styled.div<SpaceProps>`
	${space};
`;

const NavContainer = styled.div`
	${LayoutCenter};
	margin-bottom: 0;
	${({ theme }) => theme.mediaQueries.sm} {
		margin-bottom: ${majorScalePx(20)};
	}
`;

const NavButtonContainer = styled.div<{ clickable?: boolean }>`
	background-color: ${({ theme, clickable }) => {
		if (clickable) return theme.colors.primaryGreen;
		return `rgba(255, 255, 255, 0.1)`;
	}};
	border-radius: 8px;
	box-shadow: 0 3px
		${({ theme, clickable }) => {
			if (clickable) return theme.colors.primaryGreenDarker;
			return `rgba(255, 255, 255, 0.1)`;
		}};
	cursor: ${({ clickable }) => (clickable ? 'pointer' : 'default')};
	height: 61px;
	margin-bottom: 3px;
	width: 64px;
	${LayoutCenter};

	> svg {
		fill: ${({ clickable, theme }) =>
			clickable ? theme.colors.gray500 : '#FFFFFF99'};
	}
`;

const LeftNavButtonContainer = styled(NavButtonContainer)`
	display: none;
	margin-right: ${majorScalePx(4)};
	${({ theme }) => theme.mediaQueries.sm} {
		display: flex;
	}
`;

const RightNavButtonContainer = styled(NavButtonContainer)`
	display: none;
	margin-left: ${majorScalePx(4)};
	${({ theme }) => theme.mediaQueries.sm} {
		display: flex;
	}
`;

const NavIndicatorContainer = styled.div`
	${LayoutCenter};
	display: none;
	${({ theme }) => theme.mediaQueries.sm} {
		display: flex;
	}
`;

const NavIndicator = styled.div<{ active?: boolean }>`
	background-color: ${({ active }) => (active ? '#0CEFA7' : '#FFFFFF1A')};
	border-radius: 8px;
	height: 14px;
	width: 100%;
`;

function getMembersPageCount(totalMembers: number): number {
	return Math.floor(totalMembers / 4) + (totalMembers % 4 > 0 ? 1 : 0);
}

function scrollToElement(elementIndex: number): void {
	const element = document.getElementById(MemberCardIdPrefix + elementIndex);
	element?.scrollIntoView({
		behavior: 'smooth',
		block: 'nearest', // With browser smooth-scrolling flag not available, block: center error
		inline: 'start',
	});
}

function TeamInfoContent(props: TeamInfoContentProps): JSX.Element {
	const { members } = props;
	const [activePage, setActivePage] = useState(0);

	const onClickLeftNav = (): void => {
		if (activePage === 0) return;
		setActivePage(activePage - 1);
		scrollToElement(Math.min((activePage - 1) * 4, members.length));
	};

	const onClickRightNav = (): void => {
		if (activePage === indicatorsCount - 1) return;
		setActivePage(activePage + 1);
		scrollToElement(Math.min((activePage + 1) * 4, members.length));
	};

	const indicatorsCount = getMembersPageCount(members.length);

	return (
		<TeamInfoContentContainer>
			<MainContentContainer>
				<NavContainer>
					<LeftNavButtonContainer
						clickable={activePage !== 0}
						onClick={onClickLeftNav}
					>
						<ChevronLeftIcon width={32} />
					</LeftNavButtonContainer>
					<BoxContainer>
						<ListMembersContainer>
							{members.map((member, index) => {
								return (
									<MemberContainer
										key={member.id}
										id={MemberCardIdPrefix + index}
										ml={index !== 0 ? majorScalePx(6) : 0}
									>
										<TeamMemberCard
											imageSrc={member.imageSrc}
											link={member.link}
											name={member.name}
											position={member.position}
										/>
									</MemberContainer>
								);
							})}
						</ListMembersContainer>
					</BoxContainer>
					<RightNavButtonContainer
						clickable={activePage !== indicatorsCount - 1}
						onClick={onClickRightNav}
					>
						<ChevronRightIcon width={32} />
					</RightNavButtonContainer>
				</NavContainer>
				<NavIndicatorContainer>
					<GridLayout col={indicatorsCount} gap={16} width={'280px'}>
						{[...Array(indicatorsCount)].map((_, index) => (
							<NavIndicator key={index} active={activePage === index} />
						))}
					</GridLayout>
				</NavIndicatorContainer>
			</MainContentContainer>
		</TeamInfoContentContainer>
	);
}

export default TeamInfoContent;
