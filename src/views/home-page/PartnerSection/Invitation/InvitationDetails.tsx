import styled from 'styled-components';
import { AppUrl } from 'configs/constants/common';
import { Button } from 'components/Button';
import { Typography } from 'components/Typography';
import { majorScalePx } from 'components/utils';
import InvitationHeadline from './InvitationHeadline';

const InvitationDetailsContainer = styled.div`
	float: none;
	height: 100%;
	padding-top: ${majorScalePx(8)};
	width: 100%;
	${({ theme }) => theme.mediaQueries.sm} {
		float: left;
		padding-top: ${majorScalePx(24)};
		width: 50%;
	}
`;

const ContentLayout = styled.div`
	padding-left: ${majorScalePx(4)};
	padding-right: ${majorScalePx(4)};
	${({ theme }) => theme.mediaQueries.sm} {
		padding-left: 0;
		padding-right: 0;
	}
`;

const Description = styled(ContentLayout)`
	margin-bottom: ${majorScalePx(8)};
	${({ theme }) => theme.mediaQueries.sm} {
		margin-top: ${majorScalePx(38)};
	}
`;

const ButtonContainer = styled(ContentLayout)``;

function InvitationDetails(): JSX.Element {
	const onClickPreorderButton = (): void => {
		window.open(AppUrl.PREORDER as string, '_blank');
	};

	return (
		<InvitationDetailsContainer>
			<InvitationHeadline />
			<Description>
				<Typography variant={'p1'}>
					Many adorable monsters’ eggs are waiting for their owner. By
					participating in our pre-order event, you will have a chance to early
					adopt and build up your invincible squad
				</Typography>
			</Description>
			<ButtonContainer>
				<Button onClick={onClickPreorderButton}>PRE-ORDER NOW</Button>
			</ButtonContainer>
		</InvitationDetailsContainer>
	);
}

export default InvitationDetails;
