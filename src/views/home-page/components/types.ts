import { ReactNode } from 'react';

export type SectionHeaderProps = {
	backgroundName: string; // Path: public/static/images/ + filename.extension
	title: string;
};

export type SocialsItem = {
	id: number | string;
	name: string;
	link: string;
};

export type SocialEggItemProps = {
	item: SocialsItem & { logo: ReactNode };
};
