import { HelmetProvider } from 'react-helmet-async';
import { ThemeProvider } from 'styled-components';
import GlobalStyle from 'style/Global';
import { dark } from 'components/theme';
import dynamic from 'next/dynamic';
import LoadingPage from 'views/LoadingPage';

const HomePage = dynamic(() => import('./HomePage'), {
	loading: function LoadPage() {
		return <LoadingPage />;
	},
	ssr: false,
});
function Root(): JSX.Element {
	return (
		<HelmetProvider>
			<ThemeProvider theme={dark}>
				<GlobalStyle />
				<HomePage />
			</ThemeProvider>
		</HelmetProvider>
	);
}

export default Root;
