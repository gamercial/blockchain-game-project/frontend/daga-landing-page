import styled from 'styled-components';
import { BoxContainer } from 'components/Box';
import { LayoutCenter, majorScalePx } from 'components/utils';
import { getPublicImageResource } from 'utils/getResource';
import { GameplayIntroduction } from './GameplayIntroduction';
import { GameCards } from './GameCards';

const IntroductionSectionContainer = styled.section`
	background-image: url(${getPublicImageResource(
		'main-background.webp'
	)});
	background-size: contain;
	background-repeat: repeat;
	background-position: center;
	height: auto;
	padding-top: ${majorScalePx(16)};
	width: 100%;
	${({ theme }) => theme.mediaQueries.sm} {
		padding-top: ${majorScalePx(30)};
	}
`;

const MainContentContainer = styled.div`
	${LayoutCenter};
	margin-bottom: ${majorScalePx(20)};
	${({ theme }) => theme.mediaQueries.sm} {
		margin-bottom: ${majorScalePx(40)};
	}
`;

function IntroductionSection() {
	return (
		<IntroductionSectionContainer>
			<MainContentContainer>
				<BoxContainer>
					<GameplayIntroduction />
				</BoxContainer>
			</MainContentContainer>
			<GameCards />
		</IntroductionSectionContainer>
	);
}

export default IntroductionSection;
