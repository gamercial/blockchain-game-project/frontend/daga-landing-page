import { Typography } from 'components/Typography';

function PreorderTitle(): JSX.Element {
	return (
		<>
			<div>
				<Typography variant={'h1'}>ULTRA MEGA MIGHTY</Typography>
			</div>
			<Typography variant={'h1'} color={'primaryGreen'} marginRight={'12px'}>
				GALLU
			</Typography>
			<Typography variant={'h1'}>IS COMING !!!</Typography>
		</>
	);
}

export default PreorderTitle;
