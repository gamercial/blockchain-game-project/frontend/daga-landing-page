import { AppSocialsUrl } from 'configs/constants/common';
import { TwitterIcon } from 'components/Icon';

export const Socials = [
	{
		id: 1,
		name: 'Twitter',
		link: AppSocialsUrl.TWITTER as string,
		logo: <TwitterIcon color={'white'} width={28} />,
	},
];
