import styled from 'styled-components';
import { Image } from 'components/Image';
import { majorScalePx } from 'components/utils';
import { getPublicImageResource } from 'utils/getResource';
import MechanismHeader from './MechanismHeader';
import MechanismContent from './MechanismContent';
import { MechanismItems } from './constant';

const MechanismSectionContainer = styled.section`
	background-image: url(${getPublicImageResource(
		'main-background.webp'
	)});
	background-size: cover;
	background-repeat: repeat;
	background-position-y: 180px;
	position: relative;
`;

const HeroImageSize = {
	height: 386,
	width: 224,
};
const MechanismHero = styled.div`
	display: none;
	height: ${HeroImageSize.height}px;
	left: ${majorScalePx(1)};
	position: absolute;
	top: calc(-${HeroImageSize.height}px + 225px);
	width: ${HeroImageSize.width}px;
	${({ theme }) => theme.mediaQueries.sm} {
		display: block;
	}
`;

function MechanismSection(): JSX.Element {
	return (
		<MechanismSectionContainer>
			<MechanismHero>
				<Image
					alt={'Hero'}
					height={HeroImageSize.height}
					src={getPublicImageResource('mechanism-hero.webp')}
					width={HeroImageSize.width}
				/>
			</MechanismHero>
			<MechanismHeader />
			<MechanismContent items={MechanismItems} />
		</MechanismSectionContainer>
	);
}

export default MechanismSection;
