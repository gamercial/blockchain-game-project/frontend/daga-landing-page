import React from 'react';
import styled from 'styled-components';
import MailchimpSubscribe from 'react-mailchimp-subscribe';
import { Button } from 'components/Button';
import { MailchimpUrl } from 'configs/constants/common';
import { Typography } from 'components/Typography';
import { majorScalePx } from 'components/utils';
import { DesktopParagraphsValue } from 'components/Typography/configs';
import { ParagraphVariants } from 'components/Typography/types';
import { validateEmail } from 'utils/emailValidation';

enum EInputStatus {
	DEFAULT,
	ERROR,
	LOADING,
	SUCCESS,
	WARNING,
}

type InputStatus = {
	status: typeof EInputStatus[keyof typeof EInputStatus];
	message: string;
};

function getInputStatus(
	status: typeof EInputStatus[keyof typeof EInputStatus] = EInputStatus.DEFAULT
): InputStatus {
	switch (status) {
		case EInputStatus.ERROR: {
			return {
				status: EInputStatus.ERROR,
				message: '*Please enter valid email address',
			};
		}
		case EInputStatus.LOADING: {
			return {
				status: EInputStatus.LOADING,
				message: '',
			};
		}
		case EInputStatus.SUCCESS: {
			return {
				status: EInputStatus.SUCCESS,
				message:
					'Thanks for subscribing! Check your email for a confirmation message',
			};
		}
		case EInputStatus.WARNING: {
			return {
				status: EInputStatus.WARNING,
				message: 'This email is already registered',
			};
		}
		default: {
			return {
				status: EInputStatus.DEFAULT,
				message: '',
			};
		}
	}
}

const EmailTransmissionContainer = styled.div``;

const EmailWrapper = styled.div`
	display: flex;
`;

const EmailInput = styled.input<{ status: number }>`
	border: ${({ status, theme }) => {
		if (status === EInputStatus.ERROR) {
			return `2px solid #EF4343`;
		} else if (status === EInputStatus.SUCCESS) {
			return `2px solid ${theme.colors.primaryGreen}`;
		} else if (status === EInputStatus.WARNING) {
			return `2px solid #FFAB32`;
		}
		return `none`;
	}};
	border-radius: 4px 0 0 4px;
	height: 48px;
	outline: none;
	padding-left: ${majorScalePx(4)};
	padding-right: ${majorScalePx(4)};
	width: calc(100% - 80px);
	${DesktopParagraphsValue[ParagraphVariants.P1]}

	::placeholder {
		color: ${({ theme }) => theme.colors.gray300};
	}
`;

const EmailNotificationTextContainer = styled.div<{ visible: boolean }>`
	display: ${({ visible }) => (visible ? 'block' : 'none')};
	margin-top: ${majorScalePx(2)};
`;

function EmailInputWithSubmission(props: {
	message: string | null;
	status: string;
	subscribe: any;
}): JSX.Element {
	const { status, subscribe, message } = props;
	const lastStatusPropRef = React.useRef(status);
	const emailValueRef = React.useRef('');
	const [inputStatus, setInputStatus] = React.useState(getInputStatus());

	const onEmailChanged = (event: React.ChangeEvent<HTMLInputElement>): void => {
		emailValueRef.current = event.target.value;
		if (inputStatus.status !== EInputStatus.DEFAULT) {
			setInputStatus(getInputStatus(EInputStatus.DEFAULT));
		}
	};

	const onSubmitEmail = (onValidated: any): void => {
		if (!emailValueRef.current) return;
		if (validateEmail(emailValueRef.current)) {
			onValidated({ EMAIL: emailValueRef.current });
		} else {
			setInputStatus(getInputStatus(EInputStatus.ERROR));
		}
	};

	React.useEffect(() => {
		if (status === lastStatusPropRef.current) return;
		lastStatusPropRef.current = status;
		if (status == null) {
			setInputStatus(getInputStatus(EInputStatus.DEFAULT));
		} else if (status === 'sending') {
			setInputStatus(getInputStatus(EInputStatus.LOADING));
		} else if (status === 'error') {
			if (message?.includes('already subscribed')) {
				setInputStatus(getInputStatus(EInputStatus.WARNING));
			} else {
				setInputStatus(getInputStatus(EInputStatus.ERROR));
			}
		} else if (status === 'success') {
			setInputStatus(getInputStatus(EInputStatus.SUCCESS));
		}
	}, [status, message]);

	const notificationTextVisible = !!inputStatus.message;
	let notificationTextColor = undefined;
	if (inputStatus.status === EInputStatus.ERROR) {
		notificationTextColor = '#EF4343';
	} else if (inputStatus.status === EInputStatus.SUCCESS) {
		notificationTextColor = 'primaryGreen';
	} else if (inputStatus.status === EInputStatus.WARNING) {
		notificationTextColor = '#FFAB32';
	}

	return (
		<>
			<EmailWrapper>
				<EmailInput
					placeholder={'Your Email'}
					onChange={onEmailChanged}
					status={inputStatus.status}
				/>
				<Button
					borderTopLeftRadius={0}
					borderBottomLeftRadius={0}
					disabled={inputStatus.status === EInputStatus.LOADING}
					onClick={() => {
						onSubmitEmail(subscribe);
					}}
					paddingLeft={24}
					paddingRight={24}
				>
					Send
				</Button>
			</EmailWrapper>
			<EmailNotificationTextContainer visible={notificationTextVisible}>
				<Typography variant={'p1'} color={notificationTextColor}>
					{inputStatus.message}
				</Typography>
			</EmailNotificationTextContainer>
		</>
	);
}

function EmailTransmission() {
	return (
		<EmailTransmissionContainer>
			<MailchimpSubscribe
				url={MailchimpUrl}
				render={(props: any) => {
					const { subscribe, status, message } = props;
					return (
						<EmailInputWithSubmission
							message={message}
							status={status}
							subscribe={subscribe}
						/>
					);
				}}
			/>
		</EmailTransmissionContainer>
	);
}

export default EmailTransmission;
