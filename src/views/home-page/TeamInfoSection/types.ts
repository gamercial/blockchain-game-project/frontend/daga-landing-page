export type TeamInfoSectionProps = {};

export type TeamMemberCardProps = Omit<MemberInfo, 'id'>;

export type TeamInfoContentProps = {
	members: Array<MemberInfo>;
};

export type MemberInfo = {
	id: number | string;
	imageSrc: string;
	link: string;
	name: string;
	position: string;
};
