import { Contract, ContractFunction, ethers } from 'ethers';

export interface DagaSystemInfoContract extends Contract {
	getSaleOpenTime: ContractFunction<ethers.BigNumber>;
}
