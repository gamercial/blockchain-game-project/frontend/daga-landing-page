import SectionHeader from '../components/SectionHeader';

function HowToPlayHeader(): JSX.Element {
	return (
		<SectionHeader
			backgroundName={'how-to-play-section-background.png'}
			title={'HOW TO PLAY'}
		/>
	);
}

export default HowToPlayHeader;
