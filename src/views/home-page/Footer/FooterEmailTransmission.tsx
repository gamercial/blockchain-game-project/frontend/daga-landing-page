import React from 'react';
import styled from 'styled-components';
import { Typography } from 'components/Typography';
import { majorScalePx } from 'components/utils';
import EmailTransmission from './EmailTransmission';

const FooterEmailTransmissionContainer = styled.div`
	max-width: 360px;
	width: 100%;
	${({ theme }) => theme.mediaQueries.sm} {
		margin-left: ${majorScalePx(7)};
	}
`;

const Title = styled.div`
	margin-bottom: ${majorScalePx(8)};
	${({ theme }) => theme.mediaQueries.sm} {
		margin-bottom: ${majorScalePx(5)};
	}
`;

const Description = styled.div`
	margin-bottom: ${majorScalePx(6)};
	${({ theme }) => theme.mediaQueries.sm} {
		margin-bottom: ${majorScalePx(7)};
	}
`;

function FooterEmailTransmission(): JSX.Element {
	return (
		<FooterEmailTransmissionContainer>
			<Title>
				<Typography variant={'h4'}>Interested in our game ?</Typography>
			</Title>
			<Description>
				<Typography variant={'p1'} color={'gray300'}>
					Don’t be shy! Leave your email here to have a little conversation
				</Typography>
			</Description>
			<EmailTransmission />
		</FooterEmailTransmissionContainer>
	);
}

export default FooterEmailTransmission;
