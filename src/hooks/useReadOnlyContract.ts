// Code below migrated from Exchange useContract.ts

import { Contract } from 'ethers';
import { useMemo } from 'react';
import { getDagaSystemInfoContract } from 'utils/contractHelpers';
import { getReadOnlyContract } from 'utils/getReadOnlyContract';
import useActiveWeb3React from './useActiveWeb3React';

// returns null on errors
export function useReadOnlyContract(
	address: string | undefined,
	ABI: any,
	withSignerIfPossible = true
): Contract | null {
	const { library } = useActiveWeb3React();

	return useMemo(() => {
		if (!address || !ABI || !library) return null;
		try {
			return getReadOnlyContract(address, ABI, library);
		} catch (error) {
			console.error('Failed to get contract', error);
			return null;
		}
	}, [address, ABI, library]);
}

export const useDagaSystemInfoContract = () => {
	return useMemo(() => getDagaSystemInfoContract(), []);
};
