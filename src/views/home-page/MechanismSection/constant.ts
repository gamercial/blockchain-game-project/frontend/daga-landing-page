export const MechanismItems = [
	{
		id: '1',
		title: 'NFT icing',
		description:
			'Bring an ability to receive a real-life models of your NFT monsters, interact with your pets via AR,' +
			' and get familiar with a new generation of web.',
	},
	{
		id: '2',
		title: 'Cross-chain marketplace',
		description:
			'Get Gallu Monsters and other extraordinary NFTs that are unique on Solana, Binance Smart Chain,' +
			' Ethereum and various other blockchains.',
	},
	{
		id: '3',
		title: 'Staking',
		description:
			'Stake $GMON to receive additional $GMON rewards, items, and other advantages in-game.',
	},
	{
		id: '4',
		title: 'Ethereum exodus',
		description:
			'Provide a significant performance improvement and good wallet provider with neat UI and well-structured module.',
	},
];
