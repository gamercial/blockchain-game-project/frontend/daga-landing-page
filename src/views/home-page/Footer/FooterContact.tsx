import styled from 'styled-components';
import { Typography } from 'components/Typography';
import { majorScalePx } from 'components/utils';

const FooterContactContainer = styled.div`
	margin-bottom: ${majorScalePx(18)};
	max-width: 360px;
	${({ theme }) => theme.mediaQueries.sm} {
		margin-bottom: 0;
	}
`;

const Title = styled.div`
	margin-bottom: ${majorScalePx(8)};
`;

const Address = styled.div`
	margin-bottom: ${majorScalePx(2)};
`;

const Email = styled.div`
	margin-bottom: ${majorScalePx(2)};
`;

const Phone = styled.div`
	margin-bottom: ${majorScalePx(2)};
`;

function FooterContact(): JSX.Element {
	return (
		<FooterContactContainer>
			<Title>
				<Typography variant={'h4'}>Contact info</Typography>
			</Title>
			<Address>
				<Typography variant={'p1'} color={'gray300'}>
					43R/19 Ho Van Hue Street, Phu Nhuan District, HCM City.
				</Typography>
			</Address>
			<Email>
				<Typography variant={'p1'} color={'gray300'}>
					Email : contact@gallumon.com
				</Typography>
			</Email>
			<Phone>
				<Typography variant={'p1'} color={'gray300'}>
					Phone number : (+84) 911 751 725
				</Typography>
			</Phone>
		</FooterContactContainer>
	);
}

export default FooterContact;
