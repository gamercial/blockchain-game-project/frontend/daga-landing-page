import styled from 'styled-components';
import { PaddingXResponsive } from 'views/home-page/constant';
import { LayoutCenter, majorScalePx } from 'components/utils';
import { PercentageBox } from 'components/Box';
import { Image } from 'components/Image';
import { getPublicImageResource } from 'utils/getResource';

const InvitationPreviewContainer = styled.div`
	${LayoutCenter};
	float: none;
	height: 100%;
	margin-top: ${majorScalePx(8)};
	width: 100%;
	${PaddingXResponsive}
	${({ theme }) => theme.mediaQueries.sm} {
		float: right;
		margin-top: 0;
		padding-left: ${majorScalePx(30)};
		padding-bottom: ${majorScalePx(0)};
		padding-right: ${majorScalePx(0)};
		width: 50%;
	}
`;

function InvitationPreview(): JSX.Element {
	return (
		<InvitationPreviewContainer>
			<PercentageBox
				minScreenWidth={375}
				maxScreenWidth={1440}
				pHeight={[373, 497]}
				pWidth={[343, 456]}
			>
				<Image
					alt={'Hero3'}
					height={497}
					src={getPublicImageResource('partner-hero-3.webp')}
					width={456}
				/>
			</PercentageBox>
		</InvitationPreviewContainer>
	);
}

export default InvitationPreview;
