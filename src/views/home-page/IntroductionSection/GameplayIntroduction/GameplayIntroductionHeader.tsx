import styled from 'styled-components';
import { getPublicImageResource } from 'utils/getResource';
import { majorScalePx } from 'components/utils';
import { Typography } from 'components/Typography';

const GameplayIntroductionHeaderContainer = styled.div`
	background-image: url(${getPublicImageResource(
		'game-introduction-header-background.png'
	)});
	background-size: cover;
	border-top-left-radius: 4px;
	border-bottom-left-radius: 4px;
	height: 112px;
	margin-bottom: ${majorScalePx(8)};
	padding-top: ${majorScalePx(8)};
	padding-left: ${majorScalePx(8)};
	padding-bottom: ${majorScalePx(8)};
	position: absolute;
	width: calc(100% - 16px);
	${({ theme }) => theme.mediaQueries.sm} {
		background-size: 75%;
		height: 120px;
		left: calc(50% + 13px);
		min-width: 708px;
		right: 0;
		width: 50%;
	}
`;

function GameplayIntroductionHeader() {
	return (
		<GameplayIntroductionHeaderContainer>
			<Typography color={'black'} variant={'h3'}>
				GAMEPLAY INTRODUCTION
			</Typography>
		</GameplayIntroductionHeaderContainer>
	);
}

export default GameplayIntroductionHeader;
