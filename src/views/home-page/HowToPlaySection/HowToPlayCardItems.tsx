import styled from 'styled-components';
import { BoxContainer } from 'components/Box';
import { majorScalePx } from 'components/utils';
import { LayoutCenter } from 'components/utils/commonStyled';
import { HowToPlayCardItemsProps } from './types';
import HowToPlayCardItem from './HowToPlayCardItem';

const HowToPlayCardItemsContainer = styled.div`
	padding-top: ${majorScalePx(16)};
	padding-bottom: ${majorScalePx(16)};
	padding-left: ${majorScalePx(4)};
	padding-right: ${majorScalePx(4)};
	${({ theme }) => theme.mediaQueries.sm} {
		padding-top: ${majorScalePx(30)};
		padding-bottom: ${majorScalePx(30)};
	}
	${LayoutCenter};
`;

const HowToPlayCardItemsContent = styled.div`
	overflow-x: scroll;
	::-webkit-scrollbar {
		display: none;
	}
	${({ theme }) => theme.mediaQueries.sm} {
		${LayoutCenter};
	}
`;

const HowToPlayCardItemWrapper = styled.div`
	${LayoutCenter};
`;

function HowToPlayCardItems(props: HowToPlayCardItemsProps): JSX.Element {
	const { items } = props;
	return (
		<HowToPlayCardItemsContainer>
			<BoxContainer>
				<HowToPlayCardItemsContent>
					{items.map((item, index) => (
						<HowToPlayCardItemWrapper key={item.id}>
							<HowToPlayCardItem item={item} index={index} />
						</HowToPlayCardItemWrapper>
					))}
				</HowToPlayCardItemsContent>
			</BoxContainer>
		</HowToPlayCardItemsContainer>
	);
}

export default HowToPlayCardItems;
