import styled from 'styled-components';
import { Image } from 'components/Image';
import { Typography } from 'components/Typography';
import { majorScalePx } from 'components/utils';
import { HowToPlayCardItemProps } from './types';
import { LayoutCenter } from 'components/utils/commonStyled';

const HowToPlayCardItemContainer = styled.div<{ index: number }>`
	background-color: ${({ theme }) => theme.colors.gray500};
	border-radius: 8px;
	margin-top: ${({ index }) => (index === 0 ? 0 : majorScalePx(6))};
	padding-top: ${majorScalePx(4)};
	padding-bottom: ${majorScalePx(8)};
	padding-left: ${majorScalePx(4)};
	padding-right: ${majorScalePx(4)};
	min-height: 394px;
	width: 344px;
	${({ theme }) => theme.mediaQueries.sm} {
		height: 433px;
		margin-top: 0;
		margin-left: ${({ index }) => (index === 0 ? 0 : majorScalePx(4))};
		width: 360px;
	}
`;

const HowToPlayCardItemThumb = styled.div`
	border-radius: 8px;
	margin-bottom: ${majorScalePx(8)};
`;

const HowToPlayCardItemTitle = styled.div`
	margin-bottom: ${majorScalePx(2)};
	${LayoutCenter};
`;

const HowToPlayCardItemDescription = styled.div`
	text-align: justify;
`;

function HowToPlayCardItem(props: HowToPlayCardItemProps): JSX.Element {
	const { item, index } = props;
	return (
		<HowToPlayCardItemContainer index={index}>
			<HowToPlayCardItemThumb>
				<Image
					alt={'H2PCardItem'}
					borderRadius={majorScalePx(2)}
					height={200}
					objectFit={'cover'}
					src={item.imageSrc}
					width={328}
				/>
			</HowToPlayCardItemThumb>
			<HowToPlayCardItemTitle>
				<Typography variant={'h4'}>{item.title}</Typography>
			</HowToPlayCardItemTitle>
			<HowToPlayCardItemDescription>
				<Typography variant={'p1'}>{item.description}</Typography>
			</HowToPlayCardItemDescription>
		</HowToPlayCardItemContainer>
	);
}

HowToPlayCardItem.defaultProps = {
	item: {
		imageSrc:
			'https://b-f36-zpg.zdn.vn/5046520568012069416/98f0a0929d196b473208.jpg',
		title: 'Title',
		description: 'This is a description',
	},
};
export default HowToPlayCardItem;
