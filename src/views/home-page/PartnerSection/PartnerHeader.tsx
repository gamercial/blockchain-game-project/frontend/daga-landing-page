import SectionHeader from '../components/SectionHeader';

function PartnerHeader(): JSX.Element {
	return (
		<SectionHeader
			backgroundName={'partner-section-background.png'}
			title={'OUR PARTNER'}
		/>
	);
}

export default PartnerHeader;
