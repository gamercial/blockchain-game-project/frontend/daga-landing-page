import styled from 'styled-components';
import { Image } from 'components/Image';
import { getPublicImageResource } from 'utils/getResource';

const HeroPreviewContainer = styled.div`
	float: none;
	width: 100%;
	${({ theme }) => theme.mediaQueries.sm} {
		float: right;
		max-width: 547px;
		width: 50%;
	}
`;

function HeroPreview() {
	return (
		<HeroPreviewContainer>
			<Image
				alt={'HeroPreview'}
				src={getPublicImageResource('preorder-section-hero.webp')}
				height={561}
				width={547}
			/>
		</HeroPreviewContainer>
	);
}

export default HeroPreview;
