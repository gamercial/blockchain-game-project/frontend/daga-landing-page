import styled from 'styled-components';
import { BoxContainer } from 'components/Box';
import { LayoutCenter, majorScalePx } from 'components/utils';
import { Socials } from 'views/constant';
import { PaddingXResponsive } from '../constant';
import FooterLogoAndSocials from './FooterLogoAndSocials';
import FooterContact from './FooterContact';
import FooterEmailTransmission from './FooterEmailTransmission';

const FooterContainer = styled.footer`
	background: #6904e8;
	padding-top: ${majorScalePx(16)};
	padding-bottom: ${majorScalePx(16)};

	${({ theme }) => theme.mediaQueries.sm} {
		padding-top: ${majorScalePx(24)};
		padding-bottom: ${majorScalePx(24)};
	}

	${LayoutCenter};
	${PaddingXResponsive};
`;

const FlexContainer = styled.div`
	${LayoutCenter};
	display: block;
	${({ theme }) => theme.mediaQueries.sm} {
		align-items: flex-start;
		display: flex;
		justify-content: space-between;
	}
`;

function Footer(): JSX.Element {
	return (
		<FooterContainer>
			<BoxContainer>
				<FlexContainer>
					<FooterLogoAndSocials socials={Socials} />
					<FooterContact />
					<FooterEmailTransmission />
				</FlexContainer>
			</BoxContainer>
		</FooterContainer>
	);
}

export default Footer;
