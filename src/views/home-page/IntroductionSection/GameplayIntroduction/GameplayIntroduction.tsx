import styled from 'styled-components';
import HeroPreviewIntroduction from './HeroPreviewIntroduction';
import GameplayIntroductionInfo from './GameplayIntroductionInfo';

const GameplayIntroductionContainer = styled.div`
	overflow: hidden;
`;

function GameplayIntroduction() {
	return (
		<GameplayIntroductionContainer>
			<HeroPreviewIntroduction />
			<GameplayIntroductionInfo />
		</GameplayIntroductionContainer>
	);
}

export default GameplayIntroduction;
