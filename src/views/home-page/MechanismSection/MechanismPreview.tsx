import styled from 'styled-components';
import { PercentageBox } from 'components/Box';
import { getPublicImageResource } from 'utils/getResource';
import { LayoutCenter } from 'components/utils';

const MechanismPreviewContainer = styled.div`
	${({ theme }) => theme.mediaQueries.sm} {
		position: absolute;
	}
	${LayoutCenter};
`;

const ImageContent = styled.div`
	background-image: url(${getPublicImageResource('mechanism-main.webp')});
	background-size: contain;
	background-repeat: no-repeat;
	height: 100%;
	width: 100%;
`;

function MechanismPreview(): JSX.Element {
	return (
		<MechanismPreviewContainer>
			<PercentageBox
				minScreenWidth={375}
				maxScreenWidth={1440}
				pWidth={[294, 360]}
				pHeight={[492, 602]}
			>
				<ImageContent />
			</PercentageBox>
		</MechanismPreviewContainer>
	);
}

export default MechanismPreview;
