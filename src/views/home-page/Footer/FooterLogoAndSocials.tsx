import styled from 'styled-components';
import { Image } from 'components/Image';
import { getPublicImageResource } from 'utils/getResource';
import { majorScalePx } from 'components/utils';
import GridLayout from 'components/Layout/GridLayout';
import { FooterLogoAndSocialsProps } from './types';
import SocialEggItem from '../components/SocialEggItem';

const FooterLogoAndSocialsContainer = styled.div`
	margin-bottom: ${majorScalePx(18)};
	${({ theme }) => theme.mediaQueries.sm} {
		margin-bottom: 0;
	}
`;

const FooterLogoContainer = styled.div`
	margin-bottom: ${majorScalePx(3)};
	height: 93px;
	width: 257px;
`;

const FooterSocialsContainer = styled.div`
	display: flex;
`;

function FooterLogoAndSocials(props: FooterLogoAndSocialsProps): JSX.Element {
	const { socials } = props;
	return (
		<FooterLogoAndSocialsContainer>
			<FooterLogoContainer>
				<Image
					alt={'Logo'}
					src={getPublicImageResource('logo.png')}
					objectFit={'cover'}
					height={93}
					width={200}
				/>
			</FooterLogoContainer>
			<FooterSocialsContainer>
				<GridLayout col={{ xs: 1 }} gap={14}>
					{socials.map((social) => (
						<SocialEggItem key={social.id} item={social} />
					))}
				</GridLayout>
			</FooterSocialsContainer>
		</FooterLogoAndSocialsContainer>
	);
}

FooterLogoAndSocials.defaultProps = {
	socials: [],
};

export default FooterLogoAndSocials;
