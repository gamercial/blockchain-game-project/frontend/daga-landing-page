export type HowToPlayCardItem = {
	description: string;
	id: number | string;
	imageSrc: string;
	title: string;
};

export type HowToPlayCardItemProps = {
	item: HowToPlayCardItem;
	index: number;
};

export type HowToPlayCardItemsProps = {
	items: Array<HowToPlayCardItem>;
};
