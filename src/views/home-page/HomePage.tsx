import { useEffect, useState } from 'react';
import Page from 'components/Layout/Page';
import { Navbar } from 'components/Navbar';
import { useDagaSystemInfoContract } from 'hooks/useReadOnlyContract';
import PreorderSection from './PreorderSection';
import IntroductionSection from './IntroductionSection';
import HowToPlaySection from './HowToPlaySection';
import MechanismSection from './MechanismSection';
import { TeamInfoSection } from './TeamInfoSection';
import PartnerSection from './PartnerSection';
import Footer from './Footer';

function HomePage() {
	// const dagaSystemInfoContract = useDagaSystemInfoContract();
	const [preOrderOpenTimeInMs, setPreOrderTimeInMs] = useState<number>(0);
	const getTimerSale = async () => {
		// const saleTimeBigNum = await dagaSystemInfoContract.getSaleOpenTime();
		// setPreOrderTimeInMs(saleTimeBigNum.toNumber());
	};
	useEffect(() => {
		getTimerSale();
	}, []);
	return (
		<Page>
			<Navbar />
			<PreorderSection preorderTimeInMilSec={preOrderOpenTimeInMs} />
			<IntroductionSection />
			<HowToPlaySection />
			<MechanismSection />
			<TeamInfoSection />
			<PartnerSection />
			<Footer />
		</Page>
	);
}
export default HomePage;
