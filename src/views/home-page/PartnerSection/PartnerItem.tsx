import styled from 'styled-components';
import { Typography } from 'components/Typography';
import { LayoutCenter } from 'components/utils/commonStyled';
import { PartnerItemProps } from './types';

const PartnerItemContainer = styled.div`
	${LayoutCenter};
	background: #55535b;
	border-radius: 8px;
	height: 95px;
	width: 149px;
	${({ theme }) => theme.mediaQueries.sm} {
		height: 101px;
		width: 168px;
	}
`;

function PartnerItem(props: PartnerItemProps): JSX.Element {
	return (
		<PartnerItemContainer>
			<Typography variant={'h4'}>Partner</Typography>
		</PartnerItemContainer>
	);
}

export default PartnerItem;
