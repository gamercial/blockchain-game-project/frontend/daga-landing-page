import styled from 'styled-components';
import GridLayout from 'components/Layout/GridLayout';
import { LayoutCenter } from 'components/utils/commonStyled';
import { majorScalePx } from 'components/utils';
import MechanismPreview from './MechanismPreview';
import { MechanismContentProps } from './types';
import MechanismItem from './MechanismItem';
import { PaddingXResponsive } from '../constant';

const MechanismContentContainer = styled.div`
	padding-top: ${majorScalePx(16)};
	padding-bottom: ${majorScalePx(14)};
	width: 100%;

	${({ theme }) => theme.mediaQueries.sm} {
		padding-top: ${majorScalePx(30)};
		padding-bottom: ${majorScalePx(30)};
		${LayoutCenter};
	}

	${PaddingXResponsive};
`;

const ContentWrapper = styled.div`
	${LayoutCenter};
	margin-top: ${majorScalePx(12)};
	width: 100%;
	${({ theme }) => theme.mediaQueries.sm} {
		margin-top: 0;
	}
`;

const GridItem = styled.div<{ index: number }>`
	display: flex;
	padding-top: ${({ index }) => (index === 0 ? 0 : majorScalePx(12))};
	width: 100%;
	${({ theme }) => theme.mediaQueries.sm} {
		padding-top: ${({ index }) =>
			index === 0 || index === 1 ? 0 : majorScalePx(12)};
		justify-content: ${({ index }) =>
			index % 2 === 0 ? 'flex-start' : 'flex-end'};
	}
`;

const MechanismItemContainer = styled.div`
	${({ theme }) => theme.mediaQueries.sm} {
		max-width: 264px;
	}
`;

function MechanismContent(props: MechanismContentProps): JSX.Element {
	const { items } = props;
	return (
		<MechanismContentContainer>
			<MechanismPreview />
			<ContentWrapper>
				<GridLayout
					col={2}
					justifyItems={'center'}
					maxWidth={1128}
					width={'100%'}
					gap={12}
				>
					{items.map((item, index) => (
						<GridItem key={item.id} index={index}>
							<MechanismItemContainer>
								<MechanismItem index={index + 1} item={item} />
							</MechanismItemContainer>
						</GridItem>
					))}
				</GridLayout>
			</ContentWrapper>
		</MechanismContentContainer>
	);
}

MechanismContent.defaultProps = {
	items: [],
};

export default MechanismContent;
