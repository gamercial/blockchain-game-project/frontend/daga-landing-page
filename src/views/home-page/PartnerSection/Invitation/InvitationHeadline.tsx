import styled from 'styled-components';
import { Typography } from 'components/Typography';
import { getPublicImageResource } from 'utils/getResource';
import { majorScalePx } from 'components/utils';

const InvitationHeadlineContainer = styled.div`
	background: url(${getPublicImageResource(
		'invitation-headline-background.png'
	)});
	background-size: cover;
	background-position-x: 470px;
	border-top-right-radius: 8px;
	border-bottom-right-radius: 8px;
	height: 112px;
	margin-bottom: ${majorScalePx(8)};
	margin-right: ${majorScalePx(4)};
	padding-top: ${majorScalePx(8)};
	padding-bottom: ${majorScalePx(8)};
	padding-left: ${majorScalePx(4)};

	${({ theme }) => theme.mediaQueries.sm} {
		align-items: center;
		background-size: 38%;
		display: flex;
		justify-content: flex-end;
		height: 120px;
		margin-bottom: ${majorScalePx(5)};
		margin-right: 0;
		padding-top: ${majorScalePx(6)};
		padding-bottom: ${majorScalePx(6)};
		padding-left: ${majorScalePx(39)};
		padding-right: ${majorScalePx(37)};
		position: absolute;
		right: 50%;
		width: 100%;
	}
`;

function InvitationHeadline(): JSX.Element {
	return (
		<InvitationHeadlineContainer>
			<Typography variant={'h3'}>WHAT ARE YOU WAITING FOR ?</Typography>
		</InvitationHeadlineContainer>
	);
}

export default InvitationHeadline;
