import styled from 'styled-components';
import { Typography } from 'components/Typography';
import { majorScalePx } from 'components/utils';
import { LayoutCenter } from 'components/utils/commonStyled';
import { getPublicImageResource } from 'utils/getResource';
import { SectionHeaderProps } from './types';

const SectionHeaderContainer = styled.div<{ backgroundName: string }>`
	background-image: ${({ backgroundName }) =>
		`url('${getPublicImageResource(backgroundName)}')`};
	background-size: cover;
	background-repeat: repeat;
	background-position-x: left;
	background-position-y: top;
	height: 112px;
	padding-top: ${majorScalePx(8)};
	padding-bottom: ${majorScalePx(8)};
	${LayoutCenter}
	${({ theme }) => theme.mediaQueries.sm} {
		background-size: 50%;
		height: 120px;
	}
`;

function SectionHeader(props: SectionHeaderProps): JSX.Element {
	const { backgroundName, title } = props;
	return (
		<SectionHeaderContainer backgroundName={backgroundName}>
			<Typography variant={'h3'}>{title}</Typography>
		</SectionHeaderContainer>
	);
}

export default SectionHeader;
