import styled from 'styled-components';
import { layout, space } from 'styled-system';
import { PercentageBox } from 'components/Box';
import { GameCardItemProps } from './types';

const GameCardItemContainer = styled.div<{ index: number }>`
	margin-left: ${({ index }) => (index === 0 ? 0 : `16px`)};
	${({ theme }) => theme.mediaQueries.sm} {
		margin-left: ${({ index }) => (index === 0 ? 0 : `32px`)};
	}
	${layout};
	${space};
`;

const CardContent = styled.div<{ backgroundImage: string }>`
	background-image: url(${({ backgroundImage }) => backgroundImage});
	background-repeat: no-repeat;
	background-size: contain;
	height: 100%;
	width: 100%;
`;

function GameCardItem(props: GameCardItemProps): JSX.Element {
	return (
		<GameCardItemContainer {...props}>
			<PercentageBox
				pWidth={[209, 355]}
				pHeight={[330, 560]}
				minScreenWidth={375}
				maxScreenWidth={1440}
			>
				<CardContent backgroundImage={props.backgroundImage} />
			</PercentageBox>
		</GameCardItemContainer>
	);
}

GameCardItem.defaultProps = {
	item: {},
};

export default GameCardItem;
