import type { NextPage } from 'next';
import Head from 'next/head';
import styles from '../styles/Home.module.css';
import Root from 'views/home-page';

const Home: NextPage = () => {
	return (
		<div className={styles.container}>
			<Head>
				<title>Gallumon</title>
				<meta name="description" content="Gallumon" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0" />
				<link rel="icon" href="/favicon.png" />
				<link
					href="https://fonts.googleapis.com/css2?family=Londrina+Solid&display=swap"
					rel="stylesheet"
				/>
				<link
					href="https://fonts.googleapis.com/css2?family=DM%20Sans&display=swap"
					rel="stylesheet"
				/>
			</Head>

			<Root />
		</div>
	);
};

export default Home;
