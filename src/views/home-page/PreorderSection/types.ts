import { ReactNode } from 'react';

export type CountdownZoneProps = {
	preorderTime: string | number;
};

export type PreorderInfoProps = {
	title: ReactNode;
	description: ReactNode;
	preorderTime: string | number;
};
