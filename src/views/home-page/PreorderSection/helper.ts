const monthNames = [
	'January',
	'February',
	'March',
	'April',
	'May',
	'June',
	'July',
	'August',
	'September',
	'October',
	'November',
	'December',
];

function isValidDate(date: any) {
	return date instanceof Date && !isNaN(+date);
}
function getDateOrdinal(date: number): string {
	if (date > 3 && date < 21) return `${date}th`;
	switch (date % 10) {
		case 1:
			return `${date}st`;
		case 2:
			return `${date}nd`;
		case 3:
			return `${date}rd`;
		default:
			return `${date}th`;
	}
}

function getMonthName(month: number): string {
	return monthNames[month];
}

export function getOpenTimeRenderer(time: number | string): string {
	let dateInstance = new Date(time);
	if (!isValidDate(dateInstance)) {
		dateInstance = new Date(0);
	}
	return `${getDateOrdinal(dateInstance.getDate())}
	${getMonthName(dateInstance.getMonth())}
	${dateInstance.getFullYear()}`;
}
