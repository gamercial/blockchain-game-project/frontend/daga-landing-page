import styled from 'styled-components';
import { BoxContainer } from 'components/Box';
import { getPublicImageResource } from 'utils/getResource';
import { LayoutCenter } from 'components/utils';
import PartnerHeader from './PartnerHeader';
import PartnerList from './PartnerList';
import Invitation from './Invitation';

const PartnerSectionContainer = styled.section`
	background-image: url(${getPublicImageResource(
		'main-background.webp'
	)});
	background-size: cover;
	background-repeat: repeat;
	background-position-y: 700px;
`;

const PartnerContainer = styled.div`
	${LayoutCenter};
`;

const gen = (function* generator(index: number) {
	while (true) yield ++index;
})(0);
const Partners = Array(8).fill({ id: gen.next().value });

function PartnerSection(): JSX.Element {
	return (
		<PartnerSectionContainer>
			{/*<PartnerHeader />*/}
			<PartnerContainer>
				<BoxContainer>
					{/*<PartnerList partners={Partners} />*/}
					<Invitation />
				</BoxContainer>
			</PartnerContainer>
		</PartnerSectionContainer>
	);
}

export default PartnerSection;
