import styled from 'styled-components';
import { PurpleEggIcon } from 'components/Svg';
import { LayoutCenter } from 'components/utils/commonStyled';
import { SocialEggItemProps } from './types';

const SocialEggItemContainer = styled.a`
	cursor: pointer;
	position: relative;
	${LayoutCenter};
`;

const SocialLogoContainer = styled.div`
	position: absolute;
	${LayoutCenter};
`;

function SocialEggItem(props: SocialEggItemProps): JSX.Element {
	const { item } = props;
	return (
		<SocialEggItemContainer
			href={item.link}
			target={'_blank'}
			title={item.name}
		>
			<PurpleEggIcon />
			<SocialLogoContainer>{item.logo}</SocialLogoContainer>
		</SocialEggItemContainer>
	);
}

export default SocialEggItem;
