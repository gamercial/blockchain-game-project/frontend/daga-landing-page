import styled from 'styled-components';
import InvitationPreview from './InvitationPreview';
import InvitationDetails from './InvitationDetails';
import { majorScalePx } from 'components/utils';

const InvitationContainer = styled.div`
	padding-bottom: ${majorScalePx(14)};
	overflow: hidden;
	${({ theme }) => theme.mediaQueries.sm} {
		padding-bottom: ${majorScalePx(30)};
	}
`;

function Invitation(): JSX.Element {
	return (
		<InvitationContainer>
			<InvitationDetails />
			<InvitationPreview />
		</InvitationContainer>
	);
}

export default Invitation;
