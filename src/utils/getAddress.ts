import { Address } from 'configs/constants/types';
import { chainIds } from 'configs/constants/chainId';

export const getAddress = (address: Address): string => {
	const chainId: 97 | 56 = Number.parseInt(
		process.env.NEXT_PUBLIC_CHAIN_ID as string
	) as any;
	//? this as 56 is pretty ugly, i know
	return address[chainId]
		? address[chainId]
		: address[chainIds.bscMainNetId as 56];
};
