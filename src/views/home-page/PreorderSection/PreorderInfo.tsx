import styled from 'styled-components';
import { AppUrl } from 'configs/constants/common';
import { composedStyles, ComposedProps, majorScalePx } from 'components/utils';
import { LayoutCenter } from 'components/utils/commonStyled';
import { Button } from 'components/Button';
import { PreorderInfoProps } from './types';
import CountdownZone from './CountdownZone';

const PreorderInfoContainer = styled.div<ComposedProps>`
	box-sizing: content-box;
	float: none;
	height: 100%;
	padding-top: ${majorScalePx(8)};
	width: 100%;
	${composedStyles}
	${({ theme }) => theme.mediaQueries.sm} {
		float: left;
		max-width: 456px;
		padding-right: ${majorScalePx(31)};
		padding-top: 0;
		width: 100%;
		${LayoutCenter};
	}
`;

const Content = styled.div`
	display: inline-block;
	height: fit-content;
`;

// TODO: Define Header text or Text comp
const Title = styled.div<ComposedProps>`
	margin-bottom: ${majorScalePx(6)};
	${composedStyles}
`;

const Description = styled.div<ComposedProps>`
	margin-bottom: ${majorScalePx(10)};
	${composedStyles}
`;

const PreorderButton = styled.button<ComposedProps>`
	background: rgba(196, 196, 196, 1);
	padding: ${majorScalePx(3)};
	${composedStyles};
`;

function PreorderInfo(props: PreorderInfoProps) {
	const { title, description, preorderTime = 0 } = props;
	const hasFetchedPreOrderTime = preorderTime !== 0;

	const onClickPreorderButton = (): void => {
		window.open(AppUrl.PREORDER as string, '_blank');
	};

	return (
		<PreorderInfoContainer>
			<Content>
				<Title>{title}</Title>
				<Description>{description}</Description>
				{hasFetchedPreOrderTime ? (
					<CountdownZone preorderTime={preorderTime} />
				) : (
					<span>loading...</span>
				)}
				<Button onClick={onClickPreorderButton} variant={'primary'}>
					PRE-ORDER NOW
				</Button>
			</Content>
		</PreorderInfoContainer>
	);
}

export default PreorderInfo;
