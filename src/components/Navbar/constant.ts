import { SubMenuItem } from './types';
import { AppUrl } from 'configs/constants/common';

export const SubMenuItems: Array<SubMenuItem> = [
	{
		text: 'HOME',
		onClick: () => {
			window.location.href = AppUrl.HOME as string;
		},
	},
	{
		text: 'MARKETPLACE',
		onClick: () => {
			window.location.href = AppUrl.MARKETPLACE as string;
		},
	},
	{
		text: 'COMMUNITY',
		onClick: () => {
			window.location.href = AppUrl.COMMUNITY as string;
		},
	},
	{
		text: 'WHITE PAPER',
		onClick: () => {
			window.location.href = AppUrl.WHITE_PAPER as string;
		},
	},
	{
		text: 'DEMO GAMEPLAY',
		onClick: () => {
			window.location.href = AppUrl.DEMO_GAMEPLAY as string;
		},
	},
];
