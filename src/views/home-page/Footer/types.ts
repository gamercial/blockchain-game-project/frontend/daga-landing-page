import { SocialEggItemProps } from '../components/types';

export type FooterLogoAndSocialsProps = {
	socials: Array<SocialEggItemProps['item']>;
};
