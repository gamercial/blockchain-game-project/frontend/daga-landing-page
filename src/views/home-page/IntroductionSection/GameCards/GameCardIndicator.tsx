import styled from 'styled-components';
import { GameCardIndicatorProps } from './types';
import { LayoutCenter, majorScalePx } from 'components/utils';

const GameCardIndicatorContainer = styled.div`
	display: none;
	${({ theme }) => theme.mediaQueries.sm} {
		${LayoutCenter};
		margin-top: ${majorScalePx(12)};
		width: 100%;
	}
`;

const GameCardIndicatorItem = styled.div<{ index: number }>`
	background: none;
	border-radius: 50%;
	height: 34px;
	width: 34px;
	margin-left: ${({ index }) => (index === 0 ? 0 : majorScalePx(4))};
`;

function GameCardIndicator(props: GameCardIndicatorProps): JSX.Element {
	return (
		<GameCardIndicatorContainer>
			{[...Array(props.count)].map((item, index) => (
				<GameCardIndicatorItem key={index} index={index} />
			))}
		</GameCardIndicatorContainer>
	);
}

GameCardIndicator.defaultProps = {
	count: 4,
};

export default GameCardIndicator;
