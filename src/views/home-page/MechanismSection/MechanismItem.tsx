import styled from 'styled-components';
import { Typography } from 'components/Typography';
import { GreenEggIcon } from 'components/Svg';
import { majorScalePx } from 'components/utils';
import { LayoutCenter } from 'components/utils/commonStyled';
import { MechanismItemProps } from './types';

const EggTitleContainer = styled.div`
	display: flex;
	margin-bottom: ${majorScalePx(6)};
`;

const EggTitleContent = styled.div`
	position: relative;
	${LayoutCenter};
`;

const EggIndexContainer = styled.div`
	position: absolute;
`;

function getEggIndexRenderer(index: number): string {
	if (index < 10) return '0' + index;
	return index.toString();
}

function EggTitle(props: { index: number }): JSX.Element {
	return (
		<EggTitleContainer>
			<EggTitleContent>
				<GreenEggIcon />
				<EggIndexContainer>
					<Typography variant={'h3'} color={'black'}>
						{getEggIndexRenderer(props.index)}
					</Typography>
				</EggIndexContainer>
			</EggTitleContent>
		</EggTitleContainer>
	);
}

const MechanismItemContainer = styled.div``;

const Title = styled.div``;

const Description = styled.div``;

function MechanismItem(props: MechanismItemProps): JSX.Element {
	const { index, item } = props;
	return (
		<MechanismItemContainer>
			<EggTitle index={index} />
			<Title>
				<Typography variant={'h4'}>{item.title}</Typography>
			</Title>
			<Description>
				<Typography variant={'p1'}>{item.description}</Typography>
			</Description>
		</MechanismItemContainer>
	);
}

export default MechanismItem;
