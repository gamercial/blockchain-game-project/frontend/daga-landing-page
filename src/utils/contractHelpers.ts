import { ethers } from 'ethers';
import dagaSystemInfoAbi from 'configs/abi/DagaSystemInfo.json';
import { simpleRpcProvider } from './providers';
import { getDagaSystemInfoAddress } from './addressHelper';
import { DagaSystemInfoContract } from './types';

const getContract = (
	abi: any,
	address: string,
	signer?: ethers.Signer | ethers.providers.Provider
) => {
	const signerOrProvider = signer ?? simpleRpcProvider;
	return new ethers.Contract(address, abi, signerOrProvider);
};

export const getDagaSystemInfoContract = () => {
	return getContract(
		dagaSystemInfoAbi,
		getDagaSystemInfoAddress()
	) as DagaSystemInfoContract;
};
