import { LayoutProps, SpaceProps } from 'styled-system';

export type GameCardItem = {
	id: string | number;
	fileName: string;
};

export interface GameCardItemProps extends LayoutProps, SpaceProps {
	backgroundImage: string;
	item: GameCardItem;
	index: number;
}

export type GameCardIndicatorProps = {
	count: number;
};

export interface GameCardsProps extends LayoutProps, SpaceProps {
	items: Array<GameCardItem>;
}
