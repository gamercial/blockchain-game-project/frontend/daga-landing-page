import { Contract } from '@ethersproject/contracts';
import { Web3Provider } from '@ethersproject/providers';
import { AddressZero } from '@ethersproject/constants';
import { isAddress } from 'ethers/lib/utils';

// account is optional
export function getReadOnlyContract(
	address: string,
	ABI: any,
	library: Web3Provider,
	account?: string
): Contract {
	if (!isAddress(address) || address === AddressZero) {
		throw Error(`Invalid 'address' parameter '${address}'.`);
	}

	return new Contract(address, ABI);
}
