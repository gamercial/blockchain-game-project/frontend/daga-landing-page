import styled from 'styled-components';
import { Image } from 'components/Image';
import { getPublicImageResource } from 'utils/getResource';

const LoadingPageContainer = styled.div`
	align-items: center;
	display: flex;
	justify-content: center;
	height: 100vh;
	width: 100vw;
`;

function LoadingPage() {
	return (
		<LoadingPageContainer>
			<Image
				alt={'Logo'}
				src={getPublicImageResource('logo@1x.webp')}
				height={93}
				width={200}
			/>
		</LoadingPageContainer>
	);
}

export default LoadingPage;
