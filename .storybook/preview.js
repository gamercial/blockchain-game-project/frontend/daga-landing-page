import { withThemesProvider } from 'themeprovider-storybook';
import { light, dark } from '../src/components/theme';
import ResetCSS from '../src/style/ResetCSS';

export const parameters = {
	actions: { argTypesRegex: '^on[A-Z].*' },
	controls: {
		matchers: {
			color: /(background|color)$/i,
			date: /Date$/,
		},
		expanded: true,
	},
};

const themes = [
	{
		name: 'Dark',
		backgroundColor: dark.colors.background,
		...dark,
	},
	{
		name: 'Light',
		backgroundColor: light.colors.background,
		...light,
	},
];

const globalDecorator = (StoryFn) => (
	<>
		<ResetCSS />
		<StoryFn />
	</>
);

export const decorators = [globalDecorator, withThemesProvider(themes)];
