import styled from 'styled-components';
import { composedStyles, ComposedProps, majorScalePx } from 'components/utils';
import { CountdownZoneProps } from './types';
import PreorderCountdown from 'components/Countdown/PreorderCountdown';
import { LayoutCenter } from 'components/utils/commonStyled';

const CountdownZoneContainer = styled('div')<ComposedProps>`
	${LayoutCenter};
	justify-content: start;
	max-width: ${majorScalePx(144)};
	margin-bottom: ${majorScalePx(10)};
	width: 100%;
	${composedStyles}
`;

function CountdownZone(props: CountdownZoneProps) {
	const { preorderTime } = props;

	return (
		<CountdownZoneContainer>
			<PreorderCountdown endTime={preorderTime} />
		</CountdownZoneContainer>
	);
}

export default CountdownZone;
