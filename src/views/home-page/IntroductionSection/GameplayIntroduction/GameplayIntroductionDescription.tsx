import { ReactNode } from 'react';
import styled from 'styled-components';
import { Typography } from 'components/Typography';
import { majorScalePx } from 'components/utils';

const GameplayIntroductionDescriptionContainer = styled.div`
	padding-right: ${majorScalePx(4)};
	max-width: 568px; //552 + 16
	margin-top: ${majorScalePx(40)};
	margin-bottom: ${majorScalePx(10)};
	${({ theme }) => theme.mediaQueries.sm} {
		padding-right: ${majorScalePx(4)};
	}
`;

function GameplayIntroductionDescription(props: { children: ReactNode }) {
	return (
		<GameplayIntroductionDescriptionContainer>
			<Typography variant={'p1'} color={'#C2C3CC'}>
				{props.children}
			</Typography>
		</GameplayIntroductionDescriptionContainer>
	);
}

export default GameplayIntroductionDescription;
