import styled from 'styled-components';
import { Page } from 'components/Layout';
import { Navbar } from 'components/Navbar';
import { Typography } from 'components/Typography';
import { LayoutCenter, majorScalePx } from 'components/utils';
import { Socials } from 'views/constant';
import GridLayout from 'components/Layout/GridLayout';
import SocialEggItem from '../home-page/components/SocialEggItem';

const ComingSoonContainer = styled.div`
	height: 100vh;
	padding-left: ${majorScalePx(8)};
	padding-right: ${majorScalePx(8)};
	width: 100vw;
	${LayoutCenter};
`;

const ContentContainer = styled.div``;

const TitleContainer = styled.div`
	margin-bottom: ${majorScalePx(4)};
	${LayoutCenter};
`;

const DescriptionContainer = styled.div`
	margin-bottom: ${majorScalePx(8)};
	${LayoutCenter};
`;

const SocialContainer = styled.div`
	${LayoutCenter};
`;

function ComingSoon(): JSX.Element {
	return (
		<Page>
			<Navbar />
			<ComingSoonContainer>
				<ContentContainer>
					<TitleContainer>
						<Typography variant={'h1'}>Coming Soon!</Typography>
					</TitleContainer>
					<DescriptionContainer>
						<Typography variant={'p1'} color={'gray300'} textAlign={'center'}>
							Meanwhile feel free to interact with our community
						</Typography>
					</DescriptionContainer>
					<SocialContainer>
						<GridLayout col={{ xs: 1 }} gap={20}>
							{Socials.map((social) => (
								<SocialEggItem key={social.id} item={social} />
							))}
						</GridLayout>
					</SocialContainer>
				</ContentContainer>
			</ComingSoonContainer>
		</Page>
	);
}

export default ComingSoon;
