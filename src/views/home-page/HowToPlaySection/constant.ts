import { getPublicImageResource } from 'utils/getResource';

export const CardItems = [
	{
		id: 1,
		imageSrc: getPublicImageResource('how-to-play-adopt-heroes.webp'),
		title: 'Adopt Heroes',
		description:
			'There are a total of 6 species of creatures falling into 3 main groups. Player can adopt monsters by joining our pre-order event or buy them on the martketplace later',
	},
	{
		id: 2,
		imageSrc: getPublicImageResource('how-to-play-build-team.webp'),
		title: 'Build Team',
		description:
			'You need 3 monsters to build up a team. Remember that every species has its special powers. Choose them wisely!',
	},
	{
		id: 3,
		imageSrc: getPublicImageResource('how-to-play-play2earn.webp'),
		title: 'Play and Earn',
		description: `Players will enjoy every moment through conquering quests with the main story, compete with other players and join in a lot of game's activities to get token`,
	},
];
