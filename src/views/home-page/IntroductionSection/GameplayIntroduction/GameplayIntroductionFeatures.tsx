import styled from 'styled-components';
import { layout, space, LayoutProps, SpaceProps } from 'styled-system';
import { Typography } from 'components/Typography';
import { majorScalePx } from 'components/utils';
import { getPublicImageResource } from 'utils/getResource';

const GameplayIntroductionFeaturesContainer = styled.div`
	display: flex;
	margin-bottom: ${majorScalePx(12)};
	overflow-x: scroll;
	::-webkit-scrollbar {
		display: none;
	}
`;

const GameplayIntroductionFeaturesItemContainer = styled.div<
	LayoutProps &
		SpaceProps & {
			backgroundImage: string;
		}
>`
	align-items: flex-end;
	background-image: ${({ backgroundImage }) => `url(${backgroundImage})`};
	border-radius: 8px;
	display: flex;
	justify-content: center;
	min-height: 176px;
	min-width: 176px;
	padding-bottom: ${majorScalePx(3)};
	${layout}
	${space}
`;

function GameplayIntroductionFeaturesItem(
	props: { backgroundImage: string; name: string } & LayoutProps & SpaceProps
) {
	return (
		<GameplayIntroductionFeaturesItemContainer {...props}>
			<Typography variant={'h5'}>{props.name}</Typography>
		</GameplayIntroductionFeaturesItemContainer>
	);
}

function GameplayIntroductionFeatures() {
	return (
		<GameplayIntroductionFeaturesContainer>
			<GameplayIntroductionFeaturesItem
				backgroundImage={getPublicImageResource(
					'game-introduction-turn-based.webp'
				)}
				name={'TURN-BASED GAME'}
				marginRight={'12px'}
			/>
			<GameplayIntroductionFeaturesItem
				backgroundImage={getPublicImageResource(
					'game-introduction-play2earn.webp'
				)}
				name={'PLAY 2 EARN'}
				marginRight={'12px'}
			/>
			<GameplayIntroductionFeaturesItem
				backgroundImage={getPublicImageResource(
					'game-introduction-3d-graphic.webp'
				)}
				name={'3D GRAPHIC'}
				marginRight={'12px'}
			/>
		</GameplayIntroductionFeaturesContainer>
	);
}

export default GameplayIntroductionFeatures;
