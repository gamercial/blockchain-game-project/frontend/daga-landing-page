import styled from 'styled-components';
import { getPublicImageResource } from 'utils/getResource';
import HowToPlayHeader from './HowToPlayHeader';
import HowToPlayCardItems from './HowToPlayCardItems';
import { CardItems } from './constant';

const HowToPlaySectionContainer = styled.section`
	background-image: url(${getPublicImageResource(
		'main-background.webp'
	)});
	background-size: cover;
	background-repeat: repeat;
	background-position-y: 300px;
`;

function HowToPlaySection() {
	return (
		<HowToPlaySectionContainer>
			<HowToPlayHeader />
			<HowToPlayCardItems items={CardItems} />
		</HowToPlaySectionContainer>
	);
}

export default HowToPlaySection;
