module.exports = {
	multipass: true,
	js2svg: {
		indent: 2,
		pretty: true,
	},
	plugins: [
		{
			name: 'preset-default',
			params: {
				overrides: {
					cleanupIDs: false,
				},
			},
		},
		'sortAttrs',
		{
			name: 'removeElementsByAttr',
			params: {
				id: 'a',
			},
		},
		{
			name: 'removeAttrs',
			params: {
				attrs: ['mask', 'path:fill'],
			},
		},
		'removeScriptElement',
		'removeDimensions',
		'reusePaths',
	],
};
