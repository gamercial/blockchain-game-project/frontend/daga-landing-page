import { Typography } from 'components/Typography';

function PreorderDescription(): JSX.Element {
	return (
		<div>
			<Typography variant={'h6'}>
				Take a deep dive into the world of Gallumon, a Play-and-Earn Open-world
				RPG where players Breed, Trade, and Train for Battle with theirs unique
				yet adorable creatures on the quest of collecting The Gallus Shards
			</Typography>
		</div>
	);
}

export default PreorderDescription;
