import { css, DefaultTheme } from 'styled-components';
import { majorScalePx } from 'components/utils';

export const PaddingX = majorScalePx(39);

export const PaddingXResponsive = css`
	padding-left: ${majorScalePx(4)};
	padding-right: ${majorScalePx(4)};
	${({ theme }: { theme: DefaultTheme }) => theme.mediaQueries.sm} {
		padding-left: ${majorScalePx(39)};
		padding-right: ${majorScalePx(39)};
	}
`;
