import SectionHeader from 'views/home-page/components/SectionHeader';

function TeamInfoHeader(): JSX.Element {
	return (
		<SectionHeader
			backgroundName={'team-info-section-background.png'}
			title={'OUR TEAM'}
		/>
	);
}

export default TeamInfoHeader;
