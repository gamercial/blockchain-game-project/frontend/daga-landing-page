export type PartnerItem = {
	id: number | string;
};

export type PartnerItemProps = {
	item: PartnerItem;
};

export type PartnerListProps = {
	partners: Array<PartnerItem>;
};
